<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Route::get('/all-categories','HomeController@all_categories');
Route::get('/services_view','HomeController@services_view');
Route::get('/categories','HomeController@categories');
Route::get('/contact','HomeController@contact');

Route::get('/settings','HomeController@settings');
