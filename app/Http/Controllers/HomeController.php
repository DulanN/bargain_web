<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){

        $data=[
        'result'=>DB::table('system_settings')->wherestatus(1)->get(),
        ];

        return view('index')->with($data);
    }

    public function all_categories()
    {

        $data=[
            'result'=>DB::table('system_settings')->wherestatus(1)->get(),
        ];

        return view('search')->with($data);
    }

    public function services_view()
    {
        $data=[
            'result'=>DB::table('system_settings')->wherestatus(1)->get(),
        ];

        return view('service_view')->with($data);
    }
    public function categories()
    {
        $data=[
            'result'=>DB::table('system_settings')->wherestatus(1)->get(),
        ];

        return view('categories.index')->with($data);

    }
    public function contact()
    {
        $data=[
            'result'=>DB::table('system_settings')->wherestatus(1)->get(),
        ];

        return view('contacts.contact')->with($data);
    }
    public function setting()
    {

    }
}
