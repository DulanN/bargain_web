import React, {Component} from 'react';
import ReactDOM from "react-dom";

class Category extends Component {
    render() {
        return (
                    <a href="/all-categories">
                        <div className="cate-widget">
                            <img src="assets/img/test.jpg" alt="asdasd" />
                            <div className="cate-title">
                                <h3><span><i className="fas fa-circle" />Computer</span></h3>
                            </div>
                            <div className="cate-count">
                                <i className="fas fa-clone">21</i>
                            </div>
                        </div>
                    </a>

        );
    }
}

if (document.getElementById('Category')) {
    ReactDOM.render(<Category />, document.getElementById('Category'));
}
