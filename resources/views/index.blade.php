@include('includes.nav')
<section class="hero-section">
    <div class="layer">
        <div class="home-banner"></div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="section-search">
                        <h3>World's Largest <span>Marketplace</span></h3>
                        <p>Search From Awesome Verified Ads! </p>
                        <div class="search-box">
                            <form action="search" id="search_service" method="post">
                                <input type="hidden" name="" value=""/>
                                <div class="search-input line">
                                    <i class="fas fa-tv bficon"></i>
                                    <div class="form-group mb-0">
                                        <input type="text" class="form-control common_search" name="common_search"
                                               id="search-blk" placeholder="What are you looking for?">
                                    </div>
                                </div>
                                <div class="search-input">
                                    <i class="fas fa-location-arrow bficon"></i>
                                    <div class="form-group mb-0">
                                        <input type="text" class="form-control" value="" name="user_address"
                                               id="user_address" placeholder="Your Location">
                                        <input type="hidden" value="" name="user_latitude" id="user_latitude">
                                        <input type="hidden" value="" name="user_longitude" id="user_longitude">
                                        <a class="current-loc-icon current_location" data-id="1"
                                           href="javascript:void(0);"><i class="fas fa-crosshairs"></i></a>
                                    </div>
                                </div>
                                <div class="search-btn">
                                    <button class="btn search_service" name="search" value="search" type="button">
                                        Search
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="search-cat">
                            <i class="fas fa-circle"></i>
                            <span>Popular Searches</span>
                            {{--                            popular searches--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="popular-services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Most Popular Services</h2>
                            <span>Explore the greates our services. You won’t be disappointed</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="viewall">
                            <h4><a href="<?php //echo base_url();?>all-services">View All <i class="fas fa-angle-right"></i></a></h4>
                            <span>Most Popular</span>
                        </div>
                    </div>
                </div>
                <div class="service-carousel">
                    <div class="service-slider owl-carousel owl-theme">


                        <div class="service-widget">
                            <div class="service-img">
                                <a href="<?php //echo base_url().'service-preview/'.str_replace(' ', '-', $srows['service_title']).'?sid='.md5($srows['id']);?>">
                                    <img class="img-fluid serv-img" alt="Service Image" src="assets/img/test.jpg">
                                </a>
                                <div class="item-info">
                                    <div class="service-user">
                                        <a href="#">
<!--                                            --><?php //if($provider_details['profile_img'] == '') { ?>
                                            <img src="assets/img/test.jpg">
<!--                                            --><?php //} else { ?>
                                            <img src="assets/img/test.jpg">
                                        </a>
                                        <span class="service-price">1000</span>
                                    </div>
                                    <div class="cate-list">
                                        <a class="bg-yellow" href="<?php //echo base_url().'search/'.str_replace(' ', '-',strtolower($srows['category_name'])); ?>">Clothes</a></div>
                                </div>
                            </div>
                            <div class="service-content">
                                <h3 class="title">
                                    <a href="<?php //echo base_url().'service-preview/'.str_replace(' ', '-', $srows['service_title']).'?sid='.md5($srows['id']);?>"><?php //echo ucfirst($srows['service_title']);?></a>
                                </h3>
                                <div class="rating">
                                    <?php
//                                    for($x=1;$x<=$avg_rating;$x++) {
                                        echo '<i class="fas fa-star filled"></i>';
//                                    }
//                                    if (strpos($avg_rating,'.')) {
                                        echo '<i class="fas fa-star"></i>';
//                                        $x++;
//                                    }
//                                    while ($x<=5) {
                                        echo '<i class="fas fa-star"></i>';
//                                        $x++;
//                                    }
                                    ?>
                                    <span class="d-inline-block average-rating">(<?php //echo $avg_rating?>)</span>
                                </div>
                                <div class="user-info">

                                    <div class="row">
<!--                                        --><?php //if($this->session->userdata('id') != '')
//                                        { ?>
                                        <span class="col ser-contact"><i class="fas fa-phone mr-1"></i> <span>xxxxxxxx<?=rand(00,99)?></span></span>
<!--                                        --><?php //} else { ?>
{{--                                        <span class="col ser-contact"><i class="fas fa-phone mr-1"></i> <span>xxxxxxxx<?=rand(00,99)?></span></span>--}}
{{--                                        <?php } ?>--}}
                                        <span class="col ser-location"><span><?php //echo ucfirst($srows['service_location']);?></span> <i class="fas fa-map-marker-alt ml-1"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div>
									<p class="mb-0">
										No Services Found
									</p>
								</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="category-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Featured Categories</h2>
                            <span>What do you need to find?</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="viewall">
                            <h4><a href="/all-categories">View All <i class="fas fa-angle-right"></i></a></h4>
                            <span>Featured Categories</span>
                        </div>
                    </div>
                </div>
                <div class="catsec">
                    <div class="row">

                        <div class="col-lg-4 col-md-6">
                            <a href="/all-categories">
                                <div class="cate-widget">
                                    <img src="assets/img/test.jpg" alt="asdasd">
                                    <div class="cate-title">
                                        <h3><span><i class="fas fa-circle"></i>Computer</span></h3>
                                    </div>
                                    <div class="cate-count">
                                        <i class="fas fa-clone">21</i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div id="Category" class="col-lg-4 col-md-6">

                        </div>
                        <script src="js/app.js"></script>
                        {{--                        <div class="col-lg-12">--}}
                        {{--                            <div class="category">--}}
                        {{--                                No Categories Found--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="popular-services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="heading">
                            <h2>Most Popular Services</h2>
                            <span>Explore the greates our services. You won’t be disappointed</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="viewall">
                            <h4><a href="all-services">View All <i class="fas fa-angle-right"></i></a></h4>
                            <span>Most Popular</span>
                        </div>
                    </div>
                </div>
                <div class="service-carousel">
                    <div class="service-slider owl-carousel owl-theme">
                        <div class="service-widget">
                            <div class="service-img">
                                <a href="">
                                    <img class="img-fluid serv-img" alt="Service Image"
                                         src="assets/img/test.jpg">
                                </a>
                                <div class="item-info">
                                    <div class="service-user">
                                        <a href="#">
                                            <img src="assets/img/test.jpg">
                                        </a>
                                        <span
                                            class="service-price"></span>
                                    </div>
                                    <div class="cate-list">
                                        <a class="bg-yellow"
                                           href=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="service-content">
                                <h3 class="title">
                                    <a href=""></a>
                                </h3>
                                <div class="rating">
                                    <!--                                    --><?php
                                    //                                    for ($x = 1; $x <= $avg_rating; $x++) {
                                    //                                        echo '<i class="fas fa-star filled"></i>';
                                    //                                    }
                                    //                                    if (strpos($avg_rating, '.')) {
                                    //                                        echo '<i class="fas fa-star"></i>';
                                    //                                        $x++;
                                    //                                    }
                                    //                                    while ($x <= 5) {
                                    //                                        echo '<i class="fas fa-star"></i>';
                                    //                                        $x++;
                                    //                                    }
                                    //                                    ?>
                                    <span class="d-inline-block average-rating">()</span>
                                </div>
                                <div class="user-info">

                                    <div class="row">

                                        <span class="col ser-contact"><i
                                                class="fas fa-phone mr-1"></i> <span>xxxxxxxx</span></span>

                                        <span class="col ser-contact"><i
                                                class="fas fa-phone mr-1"></i> <span>xxxxxxxx</span></span>

                                        <span
                                            class="col ser-location"><span></span> <i
                                                class="fas fa-map-marker-alt ml-1"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="service-widget">
                            <div class="service-img">
                                <a href="">
                                    <img class="img-fluid serv-img" alt="Service Image"
                                         src="assets/img/test.jpg">
                                </a>
                                <div class="item-info">
                                    <div class="service-user">
                                        <a href="#">
                                            <img src="assets/img/test.jpg">
                                        </a>
                                        <span
                                            class="service-price"></span>
                                    </div>
                                    <div class="cate-list">
                                        <a class="bg-yellow"
                                           href=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="service-content">
                                <h3 class="title">
                                    <a href=""></a>
                                </h3>
                                <div class="rating">
                                    <!--                                    --><?php
                                    //                                    for ($x = 1; $x <= $avg_rating; $x++) {
                                    //                                        echo '<i class="fas fa-star filled"></i>';
                                    //                                    }
                                    //                                    if (strpos($avg_rating, '.')) {
                                    //                                        echo '<i class="fas fa-star"></i>';
                                    //                                        $x++;
                                    //                                    }
                                    //                                    while ($x <= 5) {
                                    //                                        echo '<i class="fas fa-star"></i>';
                                    //                                        $x++;
                                    //                                    }
                                    //                                    ?>
                                    <span class="d-inline-block average-rating">()</span>
                                </div>
                                <div class="user-info">

                                    <div class="row">

                                        <span class="col ser-contact"><i
                                                class="fas fa-phone mr-1"></i> <span>xxxxxxxx</span></span>

                                        <span class="col ser-contact"><i
                                                class="fas fa-phone mr-1"></i> <span>xxxxxxxx</span></span>

                                        <span
                                            class="col ser-location"><span></span> <i
                                                class="fas fa-map-marker-alt ml-1"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--                        <div>--}}
                        {{--                            <p class="mb-0">--}}
                        {{--                                No Services Found--}}
                        {{--                            </p>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how-work">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="heading howitworks">
                    <h2>How It Works</h2>
                    <span>Aliquam lorem ante, dapibus in, viverra quis</span>
                </div>
                <div class="howworksec">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="howwork">
                                <div class="iconround">
                                    <div class="steps">01</div>
                                    <img src="assets/img/icon-1.png">
                                </div>
                                <h3>Choose What To Do</h3>
                                <p>Aliquam lorem ante, dapibus in, viverra quis, feugiat Phasellus viverra nulla ut
                                    metus varius laoreet.</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="howwork">
                                <div class="iconround">
                                    <div class="steps">02</div>
                                    <img src="assets/img/icon-2.png">
                                </div>
                                <h3>Find What You Want</h3>
                                <p>Aliquam lorem ante, dapibus in, viverra quis, feugiat Phasellus viverra nulla ut
                                    metus varius laoreet.</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="howwork">
                                <div class="iconround">
                                    <div class="steps">03</div>
                                    <img src="assets/img/icon-3.png">
                                </div>
                                <h3>Amazing Places</h3>
                                <p>Aliquam lorem ante, dapibus in, viverra quis, feugiat Phasellus viverra nulla ut
                                    metus varius laoreet.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('includes.footer')
