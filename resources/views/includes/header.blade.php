<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Dreamguy's Technologies">
    <link rel="shortcut icon" type="image/x-icon" href="">

    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/plugins/datatables/datatables.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/all.min.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/css/cropper.min.css">
    <link rel="stylesheet" href="assets/css/avatar.css">
    <link rel="stylesheet" href="assets/plugins/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/plugins/owlcarousel/owl.theme.default.min.css">

{{--    <?php if($module=='home' || $module=='services'){ ?>--}}
{{--    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui.min.css">--}}
{{--    <?php } ?>--}}

{{--    <?php if($module=='service'){ ?>--}}
{{--    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-select.min.css">--}}
{{--    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/tagsinput.css">--}}
{{--    <?php } ?>--}}
    <link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="assets/css/tagsinput.css">

    <link rel="stylesheet" href="assets/plugins/toaster/toastr.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/plugins/jquery-ui/jquery-ui.min.css">
{{--    <?php if($this->uri->segment(1)=="book-service"){ ?>--}}
{{--    <link rel="stylesheet" href="assets/plugins/jquery-ui/jquery-ui.min.css">--}}
{{--    <?php }?>--}}

    <script src="assets/js/jquery-3.5.0.min.js"></script>
    <script src="https://checkout.stripe.com/checkout.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
</head>
